<?php
class View 
{       
        function __construct($view, $data) 
        {
                $this->page = $view;
                $this->data = $data;
        }
        function render($view) 
        {
                ob_start();
                include $this->page . '.php';
        $content = ob_get_clean();
        include 'layout.php';   
        }
}

?>
