<?php
include_once "RecipeModel.php";
include_once "view.php";

$recipeList = new RecipeModel();

if(isset($_GET['action']) && $_GET['action'] == "insert")
{
$page = 'form';
$data = null;
}
elseif($_SERVER['REQUEST_METHOD'] == "POST")
{
$page = 'thanks';
$data = null;
}
else
{
$page = 'list';
$data = $recipeList->findAll();
}
$view = new View($page, $data);
$view->render($view);
?>

