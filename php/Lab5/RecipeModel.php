<?php
include_once "model.php";

class Recipe
{
	function __construct($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions)
	{
		$this->id = $id;
		$this->title = $title;
		$this->ingredient0 = $ingredient0;
		$this->ingredient1 = $ingredient1;
		$this->ingredient2 = $ingredient2;
		$this->instructions = $instructions;
	}

}
class RecipeModel extends Model
{
	function findAll()
		{
		$dummyData = array(
                    new Recipe("0", "Spaghetti", "Tomatoes", "Garlic", "Basil", "Make it good."),
                    new Recipe("1", "Pad Thai", "Rice noodles", "Tamarind", "Green onions", "Make it good."),
                    new Recipe("2", "White Bean Soup", "Navy beans", "Ham hock", "Broth", "Make it good."),
                 );
		return $dummyData;
		}
}
