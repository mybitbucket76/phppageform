<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
	</head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1> 
			
			<?php if ($_SERVER['REQUEST_METHOD'] == "GET") {?>
			
			<form action="insert.php" method="post">
	  			<fieldset>
		    		<legend>Insert a recipe</legend>
					
					<label>Title</label>
		    		<input class="input-xxlarge" type="text" placeholder="Spaghetti" name="title">
		    				    		
		    		
		    		<label>Ingredients</label>
		    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient0"></div>
		    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient1"></div>
		    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient2"></div>
		    				    		
		    		<label>Instructions</label>
		    		<div><textarea name="instructions" class="input-block-level" rows="5"></textarea></div>		    		
		    		
		    		<button type="submit" class="btn btn-primary">Submit</button>
	  			</fieldset>
			</form>
			<?php } elseif ($_SERVER['REQUEST_METHOD'] == "POST") { ?>
			  <h2>Thank you for submitting <em><?php echo htmlentities($_POST['title']); ?></em></h2>
	        <h3>Ingredients</h3>
	        <ul>
	        	<li><?php echo htmlentities($_POST['ingredient0']); ?></li>
	        	<li><?php echo htmlentities($_POST['ingredient1']); ?></li>
	        	<li><?php echo htmlentities($_POST['ingredient2']); ?></li>	        
	        </ul>
	        
	        <h3>Instructions</h3>
	        <p><?php echo htmlentities($_POST['instructions']); ?></p>
			<?php } else { echo "Something has gone wrong!!"; } ?>
		</div>
        
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
